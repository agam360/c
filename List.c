#include <stdlib.h>
// l->lastNode = (*l).lastNode
struct Node {
        struct Node * preNode;
        struct Node * nextNode;
        int value;
};
typedef struct Node Node;

typedef struct List {
        Node * firstNode;
        Node * lastNode;
        int length;
        float avg;
} List;

//Initialize a List 
static List initList(void){
    List a = {NULL, NULL, 0, 0};
    return a;
}

//Add an item to the list
static Node *  addItem(int l_pos, int value){
       Node * n = (Node *) malloc(sizeof(Node));
       List * l = (List *) l_pos;
       
       //Create new Node
       n->preNode = l->lastNode;
       n->nextNode = NULL;
       n->value = value;
       
       if (l->length == 0){
             l->firstNode = n;
             l->lastNode = n;
       }else{
           l->lastNode->nextNode = n;
           l->lastNode = n;
       }
       int length = l->length;
       float avg = l->avg;
       avg = (avg*length)/(length+1);
       avg += value/(length+1);
       l->avg = avg;
       l->length++;
       
       return n;      
}

//Remove a Node from the list by a pointer to the node
static void deleteItem(int l_pos, int node_pos){
       Node * nd = (Node *) node_pos;
      
       Node * pre_nd = nd->preNode;
       Node * next_nd = nd->nextNode;
       
       //Jump over nd
       if (next_nd != NULL){
          next_nd->preNode = pre_nd;
       }
       pre_nd->nextNode = next_nd;
       
       List * l = (List *) l_pos;
       l->length--;
       float avg = l->avg;
       avg = (avg*(l->length+1))/(l->length);
       avg -= nd->value/(l->length);
       l->avg = avg;
       free(nd);//Important   
}


