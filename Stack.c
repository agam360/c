#include <stdlib.h>

struct sNode {
       struct sNode * nextsNode;
       int value;       
};
typedef struct sNode sNode;

typedef struct Stack {
        sNode * top;
        int length;        
} Stack;

static Stack createStack(){
       Stack s;
       s.top = 0;
       s.length = 0;
       return s;       
};

//Add new item to stack
static void stackPush(int s_pos, int value){
       sNode * added = (sNode *) malloc(sizeof(Stack));
       
       added->value = value;
       Stack * s = (Stack *) s_pos; 
       
       if(s->length == 0){
             added->nextsNode = NULL;   
       }else{
             added->nextsNode = s->top;
       }
       s->top = added; 
       s->length++;    
};

//Remove from stack
static int stackPop(int s_pos){
       Stack * s = (Stack *) s_pos;
       sNode * top = (sNode *) s->top;
       int ret = top->value;
       s->top = top->nextsNode;
       
       s->length--;
       free(top);
       
       return ret;
}

//Destroy the stack
static void destroyStack(int s_pos){
       Stack * s = s_pos;
       sNode * sn;
       int i;
       for(i=0;i<s->length;i++){
               sn = s->top;
               sNode * next = sn->nextsNode;
               s->top = next;   
               free(sn);     
       }    
       s->length =0;   
}
