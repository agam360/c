#include <stdio.h>
#include <stdlib.h>
#include "List.c"
#include "Stack.c"

void p_list(int l_pos);
void p_stack(int s_pos);

int main(int argc, char *argv[])
{
  /*List lst = initList();
  Node * fn = addItem((int)&lst, 111);
  Node * ln = addItem((int)&lst, 222);
  Node * ln2 = addItem((int)&lst, 333);
  addItem((int)&lst, 444);
  p_list((int)&lst);
  printf("\n Deleting 222 Node\n");
  deleteItem((int)&lst, (int)ln);
  p_list((int)&lst);
  printf("Avg: %f\n", lst.avg);*/
  Stack s = createStack();
  stackPush((int)&s, 1);
  stackPush((int)&s, 2);
  stackPush((int)&s, 3);
  
  //printf("%d\n", (*s.top).nextsNode->nextsNode->value);
  p_stack((int)&s);
  stackPush((int)&s, 1);
  stackPush((int)&s, 2);
  stackPush((int)&s, 3);
  destroyStack(&s);
  p_stack((int)&s);
  system("PAUSE");	
  return 0;
}

void p_stack(int s_pos){
     Stack * s = (Stack *) s_pos;
     while(s->top != NULL){
          int p = stackPop((int)s);
          printf("--%d\n", p);  
     }  
     printf("Finished\n");
}


//Print a list by it's memory location
void p_list(int l_pos){
     List * l = (List *) l_pos;
     Node * i = l->firstNode;
     int counter = 0;
     while( i != NULL){
            counter++;
            printf("<Node id=%d value=%d>\n", counter, i->value);  
            i = i->nextNode;
     }
     printf("Finished array with size of: %d\n", (*l).length);
}
//
//printf("%d then %d", lst.firstNode->nextNode->preNode->value, lst.firstNode->nextNode->value);
//printf("%d then %d", lst.firstNode->nextNode->preNode->value, lst.firstNode->nextNode->value);
//(*fn).value = 123
//  printf("Node: %d\n", lst.firstNode->value);
//  printf("Node: %d\n", lst.lastNode->value);
//printf("First Node: %d, %d, %d\n", (*i).nextNode);
     /*int counter = 0;
     while((*i).nextNode != NULL){
          printf("<Node id=%d value=%d >\n", counter++, (*i).value);             
     }
     printf("-----List Size: %d-----\n", (*l).length);*/
//printf("First Node value = %d \n",(* lst.firstNode).value );
//printf("List Size = %d \n", lst.length );
